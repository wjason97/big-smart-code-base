package com.example.my.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.my.server.entity.KevinRoleEntity;

public interface KevinRoleService extends IService<KevinRoleEntity> {
    KevinRoleEntity findByUsername(String username);

    void registerUser(KevinRoleEntity model);
}
