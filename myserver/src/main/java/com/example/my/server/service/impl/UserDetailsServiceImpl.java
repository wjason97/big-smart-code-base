package com.example.my.server.service.impl;

import com.example.my.server.entity.KevinRoleEntity;
import com.example.my.server.service.KevinRoleService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private KevinRoleService kevinRoleService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        KevinRoleEntity user = kevinRoleService.findByUsername(s);
        return  user;
    }

}

