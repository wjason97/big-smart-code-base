package com.example.my.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.my.server.dao.KevinRoleMapper;
import com.example.my.server.entity.KevinRoleEntity;
import com.example.my.server.service.KevinRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KevinRoleServiceImpl extends ServiceImpl<KevinRoleMapper,KevinRoleEntity> implements KevinRoleService {

    @Autowired
    private KevinRoleMapper kevinRoleMapper;

    @Override
    public KevinRoleEntity findByUsername(String username){
        QueryWrapper<KevinRoleEntity> condition = new QueryWrapper<>();
        condition.eq("name",username);
        return this.getOne(condition);
    }

    @Override
    public void registerUser(KevinRoleEntity model){
        kevinRoleMapper.insert(model);
    }
}
