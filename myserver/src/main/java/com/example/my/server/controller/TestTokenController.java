package com.example.my.server.controller;


import com.example.my.server.config.JwtConfig;
import com.example.my.server.entity.KevinRoleEntity;
import com.example.my.server.service.KevinRoleService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/kevinTest")
public class TestTokenController {

    @Resource
    private JwtConfig jwtConfig ;

    @Resource
    private KevinRoleService kevinRoleService;

    /**
     * 需要 Token 验证的接口
     */
    @PostMapping("/info")
    public String info (){
        return "info" ;
    }

    /**
     * 根据请求头的token获取userName
     * @param request
     * @return
     */
    @GetMapping("/getUserInfo")
    public String getUserInfo(HttpServletRequest request){
        String bearerToken = request.getHeader("Authorization");
        if(bearerToken == null){
            return "token无效";
        }
        String token = bearerToken.replace(JwtConfig.TOKEN_PREFIX, "");
        String username = JwtConfig.getUsername(token);
        return username ;
    }

    @PostMapping("/register")
    public void registerUser(@RequestBody KevinRoleEntity model){
        // 记得注册的时候把密码加密一下
        model.setRole("ROLE_ADMIN");
        model.setPassword(new BCryptPasswordEncoder().encode(model.getPassword()));
        kevinRoleService.registerUser(model);
    }

}
