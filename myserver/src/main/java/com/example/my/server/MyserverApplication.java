package com.example.my.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.my.server.dao")
public class MyserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyserverApplication.class, args);
    }


}