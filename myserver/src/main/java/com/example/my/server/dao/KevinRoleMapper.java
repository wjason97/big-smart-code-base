package com.example.my.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.my.server.entity.KevinRoleEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface KevinRoleMapper extends BaseMapper<KevinRoleEntity> {
}
